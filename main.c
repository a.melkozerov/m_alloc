#include <stdio.h>

#include "m_memory.h"

int test_good_alloc_free();
int test_bad_alloc_too_many();
int test_bad_free_wrong_pointer();

int main()
{
    test_good_alloc_free();
    test_bad_alloc_too_many();
    test_bad_free_wrong_pointer();

    return 0;
}

int test_good_alloc_free()
{
    MPool pool = m_init();
    if (!pool) {
        fprintf(stdout, "Pool is not allocated\n");
        return -1;
    }

    size_t blocks = m_get_free_blocks(pool);

    for (int i = 0; i < blocks; i++) {
        void *ptr = m_malloc(pool);
        if (!ptr) {
            fprintf(stdout, "Fail when index = %d\n", i);
        }
    }

    m_uninit(&pool);

    return 0;
}

int test_bad_alloc_too_many()
{
    MPool pool = m_init();
    if (!pool) {
        fprintf(stdout, "Pool is not allocated\n");
        return -1;
    }

    size_t blocks = m_get_free_blocks(pool);

    for (int i = 0; i < blocks + 1; i++) {
        void *ptr = m_malloc(pool);
        if (!ptr) {
            fprintf(stdout, "Fail when index = %d\n", i);
        }
    }

    m_uninit(&pool);

    return 0;
}

int test_bad_free_wrong_pointer()
{
    MPool pool = m_init();
    if (!pool) {
        fprintf(stdout, "Pool is not allocated\n");
        return -1;
    }

    char *ptr = (char*)m_malloc(pool);

    m_free(ptr + 2, pool);

    m_uninit(&pool);

    return 0;
}

Building on Windows


Example build with Visual Studio 2015 for Win64:


  "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64

  mkdir .build

  cd .build

  cmake -G "Visual Studio 14 2015 Win64" ..

  msbuild m_alloc.vcxproj /p:Configuration=Release /p:Platform=x64


Running on Windows


From .build directory:


  copy ..\winpthread\pthreadVC2.dll Release

  cd Release

  m_alloc.exe

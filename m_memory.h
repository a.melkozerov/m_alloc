#ifndef M_MEMORY_H
#define M_MEMORY_H

#include <stdint.h>

typedef void *MPool;

/**
 * @brief Initialize memory pool.
 *
 * @return Valid pointer to pool is success, NULL if failed.
 */
MPool m_init();

/**
 * @brief Free memory pool.
 *
 * @param hpool pointer to pool, allocated in m_init().
 */
void m_uninit(MPool *hpool);

/**
 * @brief Returns free blocks count
 * @param hpool
 * @return
 */
size_t m_get_free_blocks(MPool *hpool);

/**
 * @brief Allocate one memory block from pool.
 *
 * @param hpool pointer to pool.
 *
 * @return pointer to allocalted block or NULL if no free blocks.
 */
void *m_malloc(MPool hpool);

/**
 * @brief Free previously allocated block.
 *
 * @param ptr pointer to allocated memory.
 * @param hpool pointer to pool.
 *
 * @return 0 if success, EINVAL if hpool is not valid, EFAULT if pointer is not valid for this pool.
 */
int m_free(void *ptr, MPool hpool);

#endif // M_MEMORY_H

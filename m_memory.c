#include "m_memory.h"
#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <pthread.h>

#ifndef M_MEMORY_POOL_SIZE
#   error "Memory pool size is not defined. Define M_MEMORY_POOL_SIZE in config file."
#endif

#if M_MEMORY_POOL_SIZE == 0
#   error "Cannot set memory pool size to 0. Define M_MEMORY_POOL_SIZE in config file."
#endif

#ifndef M_MEMORY_BLOCK_SIZE
#   error "Memory block size is not defined. Define M_MEMORY_BLOCK_SIZE in config file."
#endif

#if M_MEMORY_BLOCK_SIZE == 0
#   error "Cannot set memory block size to 0. Define M_MEMORY_POOL_SIZE in config file."
#endif

struct MMemoryPool {
    void *pool;
    uint8_t usage[M_MEMORY_POOL_SIZE];

    pthread_mutex_t mutex;
};

MPool m_init()
{
    struct MMemoryPool *hpool = (struct MMemoryPool*)malloc(sizeof(struct MMemoryPool));
    if (!hpool) {
        fprintf(stderr, "Fail allocate memory pool\n");
        return NULL;
    }
    memset(hpool, 0, sizeof(struct MMemoryPool));

    hpool->pool = malloc(M_MEMORY_POOL_SIZE * M_MEMORY_BLOCK_SIZE);
    if (!hpool->pool) {
        fprintf(stderr, "Fail allocate memory pool\n");

        free(hpool);

        return NULL;
    }
    memset(hpool->pool, 0, M_MEMORY_POOL_SIZE * M_MEMORY_BLOCK_SIZE);

    pthread_mutex_init(&hpool->mutex, NULL);

    return hpool;
}

void m_uninit(MPool *hpool)
{
    struct MMemoryPool *pool = (struct MMemoryPool*)(*hpool);

    if (!pool && !pool->pool) {
        fprintf(stderr, "Invalid memory pool\n");
        return;
    }

    pthread_mutex_destroy(&pool->mutex);

    if (pool->pool)
        free(pool->pool);

    free(pool);
    pool = NULL;
}

void *m_malloc(MPool hpool)
{
    struct MMemoryPool *pool = (struct MMemoryPool*)hpool;

    if (!pool || !pool->pool) {
        fprintf(stderr, "Invalid memory pool\n");
        return NULL;
    }

    pthread_mutex_lock(&pool->mutex);

    for (size_t index = 0; index < M_MEMORY_POOL_SIZE; ++index) {
        if (pool->usage[index])
            continue;

        pool->usage[index] = 1;

        pthread_mutex_unlock(&pool->mutex);

        return (char*)pool->pool + index * M_MEMORY_POOL_SIZE;
    }

    pthread_mutex_unlock(&pool->mutex);

    return NULL;
}

int m_free(void *ptr, MPool hpool)
{
    struct MMemoryPool *pool = (struct MMemoryPool*)hpool;

    if (!pool || !pool->pool || !ptr) {
        fprintf(stderr, "Invalid memory pool\n");
        return EINVAL;
    }

    intptr_t diff = (char*)ptr - (char*)pool->pool;

    // ptr is not valid for pool
    if (diff < 0 || diff > M_MEMORY_POOL_SIZE * M_MEMORY_BLOCK_SIZE) {
        fprintf(stderr, "Free: pointer is not in pool\n");
        return EFAULT;
    }

    // ptr is not valid for pool
    if (diff % M_MEMORY_BLOCK_SIZE) {
        fprintf(stderr, "Free: pointer is not point to begin of memory block\n");
        return EFAULT;
    }

    diff /= M_MEMORY_BLOCK_SIZE;

    pthread_mutex_lock(&pool->mutex);

    pool->usage[diff] = 0;

    pthread_mutex_unlock(&pool->mutex);

    return 0;
}

size_t m_get_free_blocks(MPool *hpool)
{
    struct MMemoryPool *pool = (struct MMemoryPool*)hpool;

    if (!pool) {
        fprintf(stderr, "Invalid memory pool\n");
        return 0;
    }

    size_t empty = 0;

    pthread_mutex_lock(&pool->mutex);

    for (size_t index = 0; index < M_MEMORY_POOL_SIZE; ++index) {
        if (!pool->usage[index])
            empty++;
    }

    pthread_mutex_unlock(&pool->mutex);

    return empty;
}
